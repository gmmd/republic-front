$('.article-body, .article-subtitle').css("font-size", "19px");

$('#articlemodal')
    .modal('setting', 'closable', false)
    .modal('attach events', '.send-article')
;

$('#commentmodal')
    .modal('setting', 'closable', false)
    .modal('attach events', '.comments-btn')
;

$('.printed-headline').hide();

function printpage() {
    // var restorepage = $('body').html();
    // var printcontent = $('body').clone();
    // $('body').empty().html(printcontent);
    $("#header").hide();
    $('.printed-headline').show();
    // $(".content-block").css("margin-top", "5px");
    window.print();
    // $('body').html(restorepage);
    // $(".content-block").css("margin-top", "130px");
    $("#header").show();
    $('.printed-headline').hide();
}

$( '.print-btn' ).click(function() {
    window.print();
});

$('.text-size-btn')
    .popup({
        popup: '.font-popup',
        inline: true,
        hoverable: true,
        position: 'bottom left',
        delay: {
            show: 300,
            hide: 800
        }
    });

$('.bigger-text').click(function () {
    $('.article-body, .article-subtitle').css("font-size", "24px");
});
$('.medium-text').click(function () {
    $('.article-body, .article-subtitle').css("font-size", "19px");
});
$('.smaller-text').click(function () {
    $('.article-body, .article-subtitle').css("font-size", "14px");
});

var url = document.location.href;

new Clipboard('.copylink', {
    text: function () {
        return url;
    }
});

$('.copied').slideUp();
$('.copylink').click(function () {
    $('.copied').slideDown(100);

    setTimeout(function () {
        $('.copied').fadeOut();
    }, 3000);
});

// $( '.bookmarked' ).hide();
// $( '.bookmark-btn' ).click(function() {
// $( '.bookmarked' ).fadeIn(1000);
// $( '.bookmarked' ).fadeOut(5000);
// });


var shareButton = document.getElementsByClassName('share-btn');


// Проверка поддержки navigator.share
if (navigator.share) {
    console.log("Congrats! Your browser supports Web Share API")
    $('.share-btn').click(function () {
        // navigator.share принимает объект с URL, title или text
        navigator.share({
            title: "",
            text: "",
            url: window.location.href
        })
            .then(function () {
                console.log("Shareing successfull")
            })
            .catch(function () {
                console.log("Sharing failed")
            })
    })
} else {
    console.log("Sorry! Your browser does not support Web Share API");
    $('.share-btn')
        .popup({
            popup: '.share-popup',
            inline: true,
            hoverable: true,
            position: 'left center',
            delay: {
                show: 300,
                hide: 800
            }
        });
}

var triggerBookmark = $(".bookmark-btn"); // It must be an `a` tag

triggerBookmark.click(function () {

    if (window.sidebar && window.sidebar.addPanel) { // Firefox <23

        window.sidebar.addPanel(document.title, window.location.href, '');

    } else if (window.external && ('AddFavorite' in window.external)) { // Internet Explorer

        window.external.AddFavorite(location.href, document.title);

    } else if (window.opera && window.print || window.sidebar && !(window.sidebar instanceof Node)) { // Opera <15 and Firefox >23
        /**
         * For Firefox <23 and Opera <15, no need for JS to add to bookmarks
         * The only thing needed is a `title` and a `rel="sidebar"`
         * To ensure that the bookmarked URL doesn't have a complementary `#` from our trigger's href
         * we force the current URL
         */
        triggerBookmark.attr('rel', 'sidebar').attr('title', document.title).attr('href', window.location.href);
        return true;

    } else { // For the other browsers (mainly WebKit) we use a simple alert to inform users that they can add to bookmarks with ctrl+D/cmd+D

        alert('Ваш браузер не поддерживает функцию добавления закладок с сайтов. Вы можете добавить эту страницу в закладки браузера нажав комбинацию клавиш ' + (navigator.userAgent.toLowerCase().indexOf('mac') != -1 ? 'Command/Cmd' : 'CTRL') + ' + D или откройте меню браузера сверху или снизу интерфейса.');

    }
    // If you have something in the `href` of your trigger
    return false;
});

// var pagetitle = $('.article-title').text();
// var pageimg = $('.article-img').attr("src");
// $('title').text(pagetitle + " REPUBLIC")
// $("meta[property='og\\:title']").attr("content", pagetitle);
// $("meta[property='og\\:image']").attr("content", pageimg);
// $("meta[property='og\\:url']").attr("content", window.location.href);