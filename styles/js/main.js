$(window).on('load', function () {
    /*setTimeout(function() {
      $('.preloader').hide();
    }, 1000);*/
    $("#menu").click(function () {
        $('.scrollmenu').toggleClass("show");
        $("#menu i").toggleClass("lni-close")
    });
});
$(".content-block").click(function () {
    $('.scrollmenu').removeClass("show");
});
var c, currentScrollTop = 0,
    navbar = $('#scroll-hide');

$(window).scroll(function () {
    var a = $(window).scrollTop();
    var b = navbar.height();

    currentScrollTop = a;
    if (screen.width > 468) {
        if (c + 1 < currentScrollTop && a > b + b) {
            navbar.stop(true, false).slideUp();
        } else if (c > currentScrollTop + 2 && !(a <= b + 2)) {
            navbar.stop(true, false).slideDown();
        }
    }
    c = currentScrollTop;
});

document.addEventListener(
    "scroll",
    function () {
        var scrollTop =
            document.documentElement["scrollTop"] || document.body["scrollTop"];
        var scrollBottom =
            (document.documentElement["scrollHeight"] ||
                document.body["scrollHeight"]) - document.documentElement.clientHeight;
        scrollPercent = scrollTop / scrollBottom * 110 + "%";
        var progress = document
            .getElementById("_progress")
        if (progress) {
            progress.style.setProperty("--scroll", scrollPercent);
        }
    },
    { passive: true }
);


$(function ($) {
    $.fn.hScroll = function (amount) {
        amount = amount || 120;
        $(this).bind("DOMMouseScroll mousewheel", function (event) {
            var oEvent = event.originalEvent,
                direction = oEvent.detail ? oEvent.detail * -amount : oEvent.wheelDelta,
                position = $(this).scrollLeft();
            position += direction > 0 ? -amount : amount;
            $(this).scrollLeft(position);
            event.preventDefault();
        })
    };
});


$(document).ready(function () {
    $('.scrollmenu').hScroll(10); // You can pass (optionally) scrolling amount
});

// Night mode function
var button = $('.night-btn');
var container = $('body');

var currentTime = new Date().getHours();
var mode = sessionStorage.getItem('mode');
console.log(mode);
/* в бекэнд этот кусок кода под комментом */
// отсюда
if (currentTime > 19 && mode == null || currentTime == 0 && mode == null || currentTime < 7 && mode == null || mode == "night") {
    container.addClass('nightmode');
    $("meta[property='theme-color']").attr("content", "#333333");
    $('.night-btn i').removeClass('lni-night');
    $('.night-btn i').addClass('lni-sun');
} else if (mode == "day") {
    container.removeClass('nightmode');
    $("meta[property='theme-color']").attr("content", "#ffffff");
    $('.night-btn i').removeClass('lni-sun');
    $('.night-btn i').addClass('lni-night');
} else {
    container.removeClass('nightmode');
    $("meta[property='theme-color']").attr("content", "#ffffff");
    $('.night-btn i').removeClass('lni-sun');
    $('.night-btn i').addClass('lni-night');
}
// до сюда

button.click(function () {
    container.toggleClass('nightmode');
    if (container.hasClass('nightmode')) {
        sessionStorage.setItem('mode', 'night');
        $('.night-btn i').removeClass('lni-night');
        $('.night-btn i').addClass('lni-sun');
    } else {
        sessionStorage.removeItem('mode');
        sessionStorage.setItem('mode', 'day');
        $('.night-btn i').removeClass('lni-sun');
        $('.night-btn i').addClass('lni-night');
    }

    var csrfToken = $('meta[name="csrf-token"]').attr("content");

    $.ajax({
        /* адрес файла-обработчика запроса */
        url: '/site/mode',
        /* метод отправки данных */
        type: 'POST',
        /* данные, которые мы передаем в файл-обработчик */
        data: { _csrf: csrfToken }
        /* что нужно сделать по факту выполнения запроса */
    }).done(function (data) {
        if (data.result) {
            console.log(data.result);
        }
    });
});


if ($('#back-to-top').length) {
    var scrollTrigger = 100, // px
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('#back-to-top').addClass('show');
            } else {
                $('#back-to-top').removeClass('show');
            }
        };
    backToTop();
    $(window).on('scroll', function () {
        backToTop();
    });
    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
}

function voted(e) {
    let card = $('.poll')[0]

    //в этом месте должен быть запрос к БД, затем исполнять код ниже

    card.classList.add('slide-up-transition')

    setTimeout(function () {
        card.classList.remove('slide-up-transition')
        card.innerHTML = '<h5 class="text-center mb-1">Спасибо за участие!</h5><hr style="width: 90%; margin: .6em auto;">'
    }, 400);

    setTimeout(() => {

        card.classList.add('slide-up-transition')
        card.style.height = 0;
    }, 2000);
}

function loadMore(e) {
    let elem = $('.load-more')[0]
    elem.innerHTML = '<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>'
    setTimeout(() => {
        elem.innerHTML = ''
    }, 1000)

}


$(".current-year").text((new Date).getFullYear());